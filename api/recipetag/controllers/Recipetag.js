'use strict';

/**
 * Recipetag.js controller
 *
 * @description: A set of functions called "actions" for managing `Recipetag`.
 */

module.exports = {

  /**
   * Retrieve recipetag records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.recipetag.search(ctx.query);
    } else {
      return strapi.services.recipetag.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a recipetag record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.recipetag.fetch(ctx.params);
  },

  /**
   * Count recipetag records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.recipetag.count(ctx.query);
  },

  /**
   * Create a/an recipetag record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.recipetag.add(ctx.request.body);
  },

  /**
   * Update a/an recipetag record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.recipetag.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an recipetag record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.recipetag.remove(ctx.params);
  }
};
